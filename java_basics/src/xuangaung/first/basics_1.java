package xuangaung.first;

public class basics_1 {
    public static void main(String[] args) {
        //打印语句
        /*
        注释和c语言是一样的。
         */

        /*
          和c语言中不同的是多了一种文档注释。
         */
        System.out.println("hello word");
        System.out.println(666);//整数
        System.out.println(99.5);//浮点数
        System.out.println('a');//字符
        System.out.println('中');//汉字也是字符，字符只能存一个
        System.out.println(' ');//也可以空字符，也就是一个空格字符
        // \n  \t  等都是和c语言
        System.out.println("ture");//布尔值  真
        System.out.println("false");//布尔值  假

        char ch = 'a';
        System.out.println(ch + 1);//a的ASCII码为97但是这里+1就变成了98.

        System.out.println(ch);//与c语言不同的是，java没有类似于占位符，格式化输出的东西，所以不能转换位，ASCII码输出，在这里，可以对其进行加减运算就会自动进行转换为ASCII码来计算，可以输出ASCII来输出。

        System.out.println('\n');//这里相当于两个换行符，println本身就换行再加上'\n'的换行作用就会继续换行。

        System.out.println(' ');//从这里的实验就可以看出

        int i1 = 0b01100001;//二进制  前面+0B或者0b
        System.out.println(i1);
        int i2 = 0141;//八进制   前面+0
        System.out.println(i2);
        int i3 = 0x61;//十六进制  前面+0X或0x
        System.out.println(i3);

        //java语言中其他的数据类型和c语言都一样，但是有一种  ‘引用数据类型’
        //string类型的字符串类型，一定那创建就不能，被改变属于字符串常量。
        //在c++ ， c#  ，java中都有

        // /*C语言中没有string类型。
        //string类型是 C++、java、VB等编程语言中的。 在java、C#中，String类是不可变的，对String类的任何改变，
        // 都是返回一个新的String类对象。string 是C++标准程序库中的一个头文件，定义了C++标准中的字符串的基本模板类std::basic_string及相关的模板类实例。
        //而在C语言中，C语言中没有字符串类型，字符串是存放在字符型数组中的，将字符串作为字符数组来处理的。
        // 为了测定字符串的实际长度，C语言规定了一个“字符串结束标志”，以字符'\0'作为结束标志 。*/
        String name = "引用数据类型";
        System.out.println(name);//打印 字符串类型的字符串常量name。


        //自动类型转换
        byte a = 20;//byte类型位，转换为
        int b = a;//就会发生自动类型转换。  8位转换位32位。  发生提升，前面补0，

        int age = 20;
        double db = age;
        System.out.println(db);//自动类型转换  输出20.0   这里与c语言中不同


        char chi = 'a';//  00000000 01100001
        int code = chi;//000000000 00000000 00000000 01100001
        System.out.println(code);  //a的ASCII码是 97。

        /*为什么要发生自动类型转换：因为存在不同类型的变量赋值给其他类型的变量
         * 自动类型转换是指：  类型范围小的变量，可以直接赋值给类型范围大的变量。*/

        //表达式的自动类型转换。
        //在表达式中，小范围类型的变量回自动转换成当前范围交大范围的类型再运算。
        //如：byte、short、char会——>转化为int——>long——>float——>double
        //表达式的最终结果类型由表达式中最高的类型决定。
        //再表达式中byte、short、char是直接转换成int类型参与运算的。
        //
        System.out.println('\n');//这句话会造成两个空行
        byte ab = 10;
        int bc = 20;
        double cd = 1.0;
        double fn = ab + bc + cd;//这里会自动类型转换为double类型打印   如果这里定义成int类型就会报错。
        System.out.println(fn);

        System.out.println('\n');//这句话会造成两个空行

        //面试题
        byte i=100;
        byte j=120;
        int k =i+j;//这里为什么用一个int类型去接收两个byte类型的和呢？ 因为，虽然这两个单独都符合大小，
        // 但是两个加到一起就会超出，byte类型的最大范围，造成错误。
        //所以用int类型。避免此类问题的出现，
        System.out.println(k);

        System.out.println('\n');//这句话会造成两个空行

        //强制类型转换
        //java的强制类型转换和c语言中的语法一样。
        //但是超过最大值的时候就 可能会出现错误
        //如将1500的int类型变量强制转换为byte类型的变量，就会发生截断，只打印后八个bit的数据，由于符号位为1是负数就会打印出-36。
        //所以要注意强制类型转换，可能会造成数据的溢出（丢失）。
        //浮点型转化为整型，直接丢掉小数部分，保留整数部分返回




    }
}
