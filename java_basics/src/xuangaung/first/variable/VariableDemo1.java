package xuangaung.first.variable;

public class VariableDemo1 {
    public static void main(String[] args) {
        //目标：学会使用变量
        double money = 6.0;
        System.out.println(money);
        //接收红包
        money = money + 4.0;
        System.out.println(money);
    }
}
