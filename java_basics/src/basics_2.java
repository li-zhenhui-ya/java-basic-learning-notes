public class basics_2 {
    public static void main(String[] args) {
        //需求：拆分三位数，把个位，十位，百分位分别输出
        int a = 942;
        System.out.println("百位");
        System.out.println(a / 100);
        System.out.println("十位");
        System.out.println(a / 10 % 10);
        System.out.println("个位");
        System.out.println(a % 10);
    }
}
