package scanner;

import java.util.Scanner;
//学会使用键盘录入
//导包就相当于，写入头文件一样，类似与c语言中的头文件
public class scannerdemo {
    public static void main(String[] args) {
        //得到一个键盘扫描器对象
        Scanner sc = new Scanner(System.in);//sc就是一个扫描器对象
        //调用sc对象的功能等待接收用户输入的数据
        int c=sc.nextInt();//这个代码会等待用户输入数据，直到用户输入数据并按了回车键   nextInt表示，输入的是一个整型值

    }
}
