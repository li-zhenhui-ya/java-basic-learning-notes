package Memory_Allocation;

public class 引用类型的方法 {
    public static void main(String[] args) {
        int[] arrs={5,10,60,80};
        change(arrs);
        System.out.println(arrs[1]);
    }
    public static void change(int[] arrs){
        System.out.println(arrs[1]);
        arrs[1] =222;
        System.out.println(arrs[1]);
    }//可以看出这里是地址的传递，在方法内改变值，也会改变方法外部原参数的值。
}
//Java中的引用类型就相当于，指针