package Memory_Allocation;

public class 打印数组内容 {
    public static void main(String[] args) {
        int[] arr = {1, 23, 4, 56};
        print_arr(arr);
    }

    public static void print_arr(int[] arr) {
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i]);
            }
        }
    }
}
