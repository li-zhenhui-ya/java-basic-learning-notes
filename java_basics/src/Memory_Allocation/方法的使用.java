package Memory_Allocation;

public class 方法的使用 {
    public static void main(String[] args) {
        int[] arr = {1, 5, 6, 10, 100, 600, 900000, 8, 9};
        int max=find(arr);
        System.out.println(max);
    }

    public static int find(int[] arr) {//这里接收的还是，数组的地址
        int max = arr[0];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
//引用类型的值传递的方法，是地址传递，如数组的传参，传的就是，地址