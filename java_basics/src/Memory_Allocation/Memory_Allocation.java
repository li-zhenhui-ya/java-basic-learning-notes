package Memory_Allocation;

public class Memory_Allocation {
    public static void main(String[] args) {
        int c1 = sum(100,200);
        System.out.println("和是："+c1);//加号相当于c语言中的逗号表达式，这里表示连接

        //Java中的方法就是c语言中的函数。

        hello();
    }

    public static int sum(int a, int b) { //方法就是c语言中的函数
        int c = a + b;
        return c;
    }
    public  static void hello(){//无返回，无参数，类型的方法
        System.out.println("hello world");
    }
}

//Java的函数，不用写在main函数的前面，也不需要声明函数。
//方法值之间不能嵌套定义  可以嵌套调用


//Java中允许使用 return 不带任何的返回值，用来结束当前函数。