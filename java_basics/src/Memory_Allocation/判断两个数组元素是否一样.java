package Memory_Allocation;

public class 判断两个数组元素是否一样 {
    public static void main(String[] args) {
        int[] arr1 = {10, 20, 30, 60, 40};
        int[] arr2 = {10, 20, 30, 60, 40};
        System.out.println(compare(arr1, arr2));

    }

    public static int compare(int[] arr1, int[] arr2) {
        if (arr1.length == arr2.length) {
            for (int i = 0; i < arr1.length; i++) {
                if (arr1[i] != arr2[i]) {
                    return -1;
                }
            }
            return 1;//是一样的
        }
        else{
            return -1;
        }

    }
}
