package Memory_Allocation;

public class 方法重载 {
    public static void main(String[] args) {
        //方法重载：就是同一个方法（函数）有不同的形参列表。
        fire();
        fire("发射10 9 8 2 1 0...");
        fire("发生321..",10);

    }
    public static void fire(){
        //System.out.println("滴，发射");
        fire("meiguo");//函数的嵌套调用
    }
    public static void fire(String location){
        //System.out.println("默认发射一枚东风");
        fire(location,1);
    }
    public static void fire(String location,int number){
        System.out.println("发生两枚东风\t"+"武器枚数\t"+number);
    }
}

//方法（函数）重载：其实就是，函数名相同但是形参列表不同的函数的定义。   函数也可以循环嵌套调用。
//Java中允许，定义名字相同但是参数列表不同的函数。
