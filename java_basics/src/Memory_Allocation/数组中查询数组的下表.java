package Memory_Allocation;

import java.util.Scanner;

public class 数组中查询数组的下表 {
    public static void main(String[] args) {
        int[] arr = {11, 22, 33, 44};
        Scanner s = new Scanner(System.in);
        int data = s.nextInt();
        int i = search(arr,data);
        System.out.println(i);
    }

    public static int search(int[] arr, int date) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == date) {
                return i;
            }
        }
        return -1;//查无此数
    }
}
