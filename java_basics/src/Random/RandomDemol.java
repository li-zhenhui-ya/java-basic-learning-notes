package Random;

import java.util.Random;

public class RandomDemol {
    //学会使用Java提供的随机数类Random
    //导包  写头文件
    //创建随机数对象
    public static void main(String[] args) {
        Random r= new Random();//new一个对象。
        //调用nexInt功能（方法）   可以返回一个整型的随机数
        for (int i=0;i<10;i++) {
            int data =r.nextInt(10);//随机生成一个0-9的随机数，不包含10 包前不包后。
            System.out.println(data);
        }
    }
}
