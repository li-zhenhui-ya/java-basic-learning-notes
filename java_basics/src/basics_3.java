public class basics_3 {
    public static void main(String[] args) {
        //+号做连接符
        int a = 5;
        System.out.println("abc" + 'a');//不能运算，连接
        System.out.println("abc" + a);//不能运算，连接
        System.out.println(5 + a);//算
        System.out.println("abc" + 5 + 'a');//不能运算，连接
        System.out.println(15 + "abc" + 15);//不能运算，连接
        System.out.println(a + 'a');//'a'：字符5再内存是ASCII码值，可以算  102
        System.out.println(a + " " + 'a');//a+""空字符串形成字符串5  ，连接成 5 a
        System.out.println(a + 'a' + "i the ima");//不能运算，连接
        System.out.println("i the ima" + a + 'a');//不能运算，连接
        System.out.println("i the ima" + (a + 'a'));//不能运算，连接


        //自加自减运算和c一样

        //java中的逻辑运算符与c语言差不多，
        //逻辑与:&:全真为真有假为假
        //逻辑或：|：有真为真，全假为假
        //逻辑非：！：真假相反
        //逻辑异或：^ 一真一假为真，同真同假为假。

        System.out.println(" ");


        //注意双与、双或和单与、单或的区别。他们两个执行的结果一样，但是双与只要前面的条件为真则为真，后面的就不再执行了。其执行的速度比单与快
        //例如：
        int j = 10;
        int k = 20;
        System.out.println(j > 100 && ++k > 10);
        System.out.println(k);//因前面为假，就不在执行后面所以，++k  并没有被执行，所以k的值并没有被改变。

        //双与的优先级高于双或
        //例如：
        System.out.println(10 > 3 || 10 > 3 && 10 < 3);//true
        System.out.println((10 > 3 || 10 > 3) && 10 < 3);//false


        //还有按位与、按位或、按位非、按位异或 （这些都是对二进制操作的）
    }
}
